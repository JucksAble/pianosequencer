<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Piano Sequencer</title>
        <link rel="stylesheet" href="style.css">
        <script src="https://unpkg.com/tone@14.7.77/build/Tone.js"></script>
    </head>
    <body>
        <ul id="piano">
            <?php
            //Number of octaves. | higher than 7 not recommended!
            $octaves = 3; 
                //array of all 12 notes
                $notes = array("C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B");
                $notes = array_reverse($notes);
                
                //loop trough the 7 full octaves
                for ($o=$octaves+1; $o > 1; $o--) {
                    //toop trough notes array
                    foreach ($notes as $note) {
                        //if a key is sharp add the black class
                        if($note == "C#" || $note == "D#" || $note == "F#" || $note == "G#" || $note == "A#"){
                            echo "<li data-note='".$note . $o ."' class='key black'></li>";
                        }else{
                            echo "<li data-note='".$note . $o ."' class='key'></li>";
                        }
                    }
                }
            ?>
        </ul>

        <table id="synth">
            <?php
            for ($o=$octaves+1; $o > 1; $o--) {
                foreach ($notes as $note) {
                    echo "<tr id='nRow'>";
                        for ($n=0; $n < 48; $n++) { 
                            echo "<td id='nSpot'><div id='note' data-note='".$note . $o ."'></div></td>";
                        }
                    echo "</tr>";
                }
            }
            ?>
        </table>
    </body>
    <script src="app.js"></script>
</html>